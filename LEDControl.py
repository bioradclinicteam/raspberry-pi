# LEDControl.py
# LED Control Using Bio-Rad Android Application
# Frances Su fsu@g.hmc.edu
# Date of most recent changes: 05/09/2014
# Receives data through Bluetooth connection and turns on transillumination LEDs.
# Starts detection of electrophoresis completion when Bio-Rad Android App says to. 
# Safely shuts down Raspberry Pi using Bio-Rad Android application.

import serial			#imports serial library
import traceback
import RPi.GPIO as GPIO		#imports GPIO library
from time import sleep		# import sleep from time library
import os			# import os library

GPIO.setmode(GPIO.BOARD) 	# Sets pin names according to the pin numbers on RPi
GPIO.setup(16, GPIO.OUT) 	# Sets up pin 12 as an output pin  backlit white light



ser = None
print "\nser"

try:
    ser = serial.Serial('/dev/ttyAMA0', baudrate =  115200, timeout = 3)
    ser.open()
    print "port open"

    while True:
        #print "loop"
        try:
            serial_data = ser.read()		#read serial data	
            ser.flushInput()
            print serial_data			#print data
            if serial_data =='1':		#if serial_data = 1, turn on transillumination LEDs
                print "on"
                GPIO.output(16,1) 		#Sets output pin 12 to high/3.3Vif
            elif serial_data=='0':		#if serial_data = 0, turn off transillumination LEDs
                print "off"
                GPIO.output(16,0)
            elif serial_data=='2':		#if serial_data = 2, start detection of electrophoresis completion
                print "motion!"
                os.system("sudo motion-mmal")
            elif serial_data=='3':		#if serial_data = 3, shut down RPi
                print "shutting down"
                os.system("sudo shutdown -h now")	#runs command in terminal to shut down RPi
        except:
            traceback.print_exc()
            #print "except"
            pass
except:
    pass
finally:
    if ser:
        ser.close()
    GPIO.cleanup()				#restores GPIO pins to initial values
            
