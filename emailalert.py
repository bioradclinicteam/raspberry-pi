# emailalert.py
# Email Alert for Gel Electrophoresis Completion
# Frances Su fsu@g.hmc.edu
# Date of most recent changes: 04/24/2014

import smtplib
import os

#Setup Email Account to Send from

smtpUser = ‘username1@mail.com’ 	#user E-mail
smtpPass = ‘password’			#E-mail password

#Email recipient
toAdd = ‘username2@mail.com’		#recipient E-mail
fromAdd = smtpUser

subject = 'Electrophoresis Test'
header = 'To:' + toAdd + '\n' + fromAdd+ '\n' + 'Subject:' + subject
body = 'Your electrophoresis is complete. Please remove your gel.'

message = 'Subject: %s\n\n%s' %(subject,body)

#Communication with smtp server

s = smtplib.SMTP('smtp.gmail.com',587) #connects to server

s.ehlo()        #Setup for encryption
s.starttls()
s.ehlo()

s.login(smtpUser,smtpPass)          # login to gmail account
s.sendmail(fromAdd,toAdd,message)  #sends email. Need 2 new lines to send body.

print 'message sent'

s.quit()    #close server

# Stopping motion
os.system("/home/pi/stopmotion")	#stops Motion software after E-mail is sent
